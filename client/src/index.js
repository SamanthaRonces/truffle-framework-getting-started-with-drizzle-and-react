import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

/** Drizzle Dizzle */
// import drizzle functions and contract artifact
import { Drizzle, generateStore } from "drizzle";
import MyStringStore from "./contracts/MyStringStore.json";

// let drizzle know what contracts we want
// Contracts are compiled from .sol files into .json files under the /build/contracts folder
// they are symlinked to the /client/src/contracts directory because React disallows inclusion of files outside of /src/
const options = { contracts: [MyStringStore] };

// setup the drizzle store and drizzle
// This sets up redux and readies a 'drizzle' instance, which we pass in as a prop to our App component.
const drizzleStore = generateStore(options);
const drizzle = new Drizzle(options, drizzleStore);
/** Drizzle Dizzle */

// pass in the drizzle instance to our App - you're good to go!
ReactDOM.render(<App drizzle={drizzle} />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
